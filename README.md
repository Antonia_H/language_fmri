

Download project by typing "git clone https://gitlab.com/<userName_gitlab>/language_fmri.git <path_to_your/project> "

Folder Structure:
put fMRI images and the LogFile in /subjects/

eg.:
	cd /subjects
	ls 
	1234 234234 345345
	ls 1234/
	SRACHE_VISUELL_3T_0005 Trial_7_CorrectTime_098098-1234.log

Run in ~/path_to_your/project:	./design_file_fMRI_ForPipeline.py -s <ID>

--> runs all commands step by step:
		- create from logFile matrices for feat-anaylsis
		- dcm2nii an save nifti in /subjects/<ID>/
		- create design file and call it <ID>.fsf
		- run feat analyis
		- create masks and open fsleyes for screenshots
		- write in commandline name, birthdate and study date of patient to create (manually) a "Befund" (with word, templates can be found in /NAS)


Hint: In case analysis aborts, check how many files were in the dicom_directory. Sometimes files are added when copying from scanner -> NAS or NAS -> your working directory. You can check for this error by comparing the number of files in your dicom-directory to the "# Total volumes" in the subject's design.fsf. 
After deleting unneded filed, additionally delete falsely created <ID>.fsf file and <ID>.nii.gz file before rerunning ./design_file_fMRI_ForPipeline.py.
