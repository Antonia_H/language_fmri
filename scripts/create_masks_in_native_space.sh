#!/bin/bash

cd $1 #innerhalb der .feat laufen lassen


#left temp_pariet
/usr/local/fsl/bin/featquery 1 $1 1  stats/zstat1 left_temp_pariet -s -w ~/Desktop/Antonia/SHK/Sprach_fMRT/administration/scripts/left_Superior_Temporal_Gyrus.nii.gz 



#right
/usr/local/fsl/bin/featquery 1 $1 1  stats/zstat1 right_temp_pariet -s -w  ~/Desktop/Antonia/SHK/Sprach_fMRT/administration/scripts/right_Superior_Temporal_Gyrus.nii.gz 



#left_broca44
/usr/local/fsl/bin/fslroi /usr/local/fsl/data/atlases//Juelich/Juelich-prob-2mm /tmp/featquery_gm86hk 12 1

/usr/local/fsl/bin/featquery 1 $1 1  stats/zstat1 left_broca44 -s -w  /tmp/featquery_gm86hk



#right_broca44
/usr/local/fsl/bin/fslroi /usr/local/fsl/data/atlases//Juelich/Juelich-prob-2mm /tmp/featquery_GYlhCN 13 1

/usr/local/fsl/bin/featquery 1 $1 1  stats/zstat1 right_broca44 -s -w  /tmp/featquery_GYlhCN


#in fsleyes öffnen für screenshot
fsleyes reg/example_func.nii.gz left_temp_pariet/mask.nii.gz -cm green right_temp_pariet/mask.nii.gz -cm green left_broca44/mask.nii.gz -cm blue right_broca44/mask.nii.gz -cm blue stats/zstat1.nii.gz -cm red -dr 1.96 6

