#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 22 15:51:39 2020

@author: aharms
"""



import pandas as pd
import os
import numpy as np 
import glob


#=========================== Adjust ===========================================



#ID = "1750147"


#==============================================================================

#inPath = '/Users/aharms/Desktop/Antonia/SHK/Sprach_fMRT/administration/LogFiles'







def prepFLogFile(inPath):
    infile =  glob.glob(inPath + "/*.log")[0]
    final_file = []
    
    with open(infile) as f:
        
        line_list = f.readlines()
        
        delete_after = line_list.index('{\n')
        line_list = line_list[:line_list.index('{\n')] # letzten Teil löschen
        line_list = line_list[[line_list.index(i) for i in line_list if 'Trial\tEvent Type\tCode\tTime' in i][0]:] # ersten Teil löschen
        
        
        
        for line, i in zip(line_list,range(len(line_list))):
        
            if line == '\n': 
                print(i)
            else:
                final_file.append(line)
    
    final_file_pd = pd.DataFrame(final_file)
    
    logfile = final_file_pd[:][0].str.split("\t", expand=True,)
    
    logfile.columns = logfile.iloc[0]
    logfile = logfile.drop([0])
    
    logfile.index  = range(0,len(logfile))
    
    return(logfile)









def getReduced(task, logfile):
    
    only_task = logfile.loc[logfile.Code.str.contains(task)]
    
    logTextFSL = pd.DataFrame(columns=['Time','Duration'])    
        
    time = []
    duration = []
    for i in range(0,len(only_task['Time']), 6): #Immer sechs Bilder pro Block
        
        time.append(only_task['Time'].iloc[i])
        duration.append(only_task['Time'].iloc[i+5] +  only_task['Duration'].iloc[i+5] - only_task['Time'].iloc[i])
    
    
    logTextFSL['Time'] = time
    logTextFSL['Duration'] = duration
    logTextFSL["ones"] = 1
    
    return(logTextFSL)

def getReduced_event(task, logfile):
    '''
    Takes every Word or Letter showing as event. Only first three seconds to react
    '''
    only_task = logfile.loc[logfile.Code.str.contains(task)]
    logTextFSL = pd.DataFrame(columns=['Time','Duration'])

    time = []
    duration = []
    
    for i in range(0,len(only_task['Time'])): #event design
        
        time.append(only_task['Time'].iloc[i])
        duration.append(3.0)
    
    
    logTextFSL['Time'] = time
    logTextFSL['Duration'] = duration
    logTextFSL["ones"] = 1    
    
    return(logTextFSL)
    

def main(ID):


    inPath = os.path.dirname(os.path.realpath(__file__)) + '/subjects/'+ID
        
    logfile = prepFLogFile(inPath)
    
    
    logfile['Time'] = logfile['Time'].astype(int)
    logfile['Time'] = logfile['Time']/10000 #time in seconds
    logfile['Duration'] = pd.to_numeric(logfile['Duration'])
    logfile['Duration'] = logfile['Duration']/10000
    
    
    
    
    subtrahent = logfile.loc[0,"Time"]
    logfile["Time"] = logfile.loc[:,"Time"] - subtrahent
    
    
    
        
    speech = getReduced('Words', logfile)
    no_speech = getReduced('Letters', logfile)
        
    # speech = getReduced_event('Words')
    # no_speech = getReduced_event('Letters')
    

    
    np.savetxt(os.path.join(inPath, 'log_speech_'+ID+'.txt'), speech.values, fmt='%1.9f')
    np.savetxt(os.path.join(inPath, 'log_no_speech_'+ID+'.txt'), no_speech.values, fmt='%1.9f')


if __name__=='__main__':
    main("12345")
    
    
    



































