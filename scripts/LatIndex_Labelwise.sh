

#get Lateralization Index:
#
cd $1 #FSL_outputs/ FSL_outputs/MNI/Felix_MNI.feat/


#fslmaths rightBroca/mask.nii.gz -add righttemp_pariet_t2/mask.nii.gz RIGHT_mask
#fslmaths leftBroca/mask.nii.gz -add lefttemp_pariet_t2/mask.nii.gz LEFT_mask


declare -a activeVoxels #erst left dann right

contrast="zstat1"

echo "Contrast: $contrast"
#ACHTUNG: Globaer Threshhold der Aktivierung


for roi in broca44 temp_pariet;
do
	
	i=0


	fslmaths left_${roi}/mask.nii.gz -add right_${roi}/mask.nii.gz bothHemi_mask
	#95% Percentil (nonzero values)
	fivepercent=$(fslstats stats/${contrast}.nii.gz -k bothHemi_mask.nii.gz  -P 95)

	##mean der 5% "meist Aktiven" Voxel
	meanmax=$(fslstats stats/${contrast}.nii.gz -k bothHemi_mask.nii.gz -l $fivepercent -M)
	#echo "Mean of all voxels above 95% percentile: $meanmax"
	#include all voxel above this threshhold
	criticalvalue=$(echo $meanmax*0.5 | bc)
	echo "Critical Threshhold of activation in ${roi} area (mean of 5% voxel with highest activation): $criticalvalue"




	for lat in left right;
		do
			
			



			#calcluate number of voxels
			voxactive=$(fslstats stats/${contrast}.nii.gz -k ${lat}_${roi}/mask.nii.gz -l $criticalvalue -V | awk '{print $1}')

			echo "Active Voxel in ${lat}_${roi} ${voxactive}"


			activeVoxels[$i]=$voxactive

			

			#i=$((i+1))
			((i++))
			#echo $i

		done

	#echo ${activeVoxels[@]}

	left=${activeVoxels[0]}
	right=${activeVoxels[1]}


	leftminusright=$(python -c "print( ($left - $right) / ($left + $right) )")
	echo "Lateralization index for ${roi}: ${leftminusright}"
done
#fslmaths leftBroca_c2/mask.nii.gz -add lefttemp_pariet_t2/mask.nii.gz LEFT_mask
#muss davor in featQuery erzeugt werden




#fslstats zstat2.nii.gz -l 3 -k ../rightBroca/mask.nii.gz -V






#/usr/local/fsl/bin/featquery 1 /Users/aharms/Desktop/Sprach_fMRT/FSL_outputs/MNI/Johannes_MNI.feat 1  stats/tstat2 lefttemp_pariet_t2 -p -s -b /Users/aharms/Desktop/Sprach_fMRT/FSL_outputs/MNI/Lefttemp_pariet.nii.gz
#/usr/local/fsl/bin/featquery 1 /Users/aharms/Desktop/Sprach_fMRT/FSL_outputs/MNI/Johannes_MNI.feat 1  stats/tstat2 righttemp_pariet_t2 -p -s -b /Users/aharms/Desktop/Sprach_fMRT/FSL_outputs/MNI/Righttemp_pariet.nii.gz

