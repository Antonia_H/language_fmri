#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 14 16:20:22 2021

@author: aharms
"""

# This script will generate each subjects design.fsf, but does not run it.
# It depends on your system how will launch feat

import os
import glob

import shutil,subprocess
from argparse import ArgumentParser

import lab_log_file_fMRI_ForPipeline as logFile

from argparse import ArgumentParser

import datetime

version='design_file_fMRI_ForPipeline.py by Antonia Harms (31.08.2021)'


'''
=========================================================================
'''
#subject = "1750147"

'''
=========================================================================
'''
def calculate_age(born, studydate):
    
    return studydate.year - born.year - ((studydate.month, studydate.day) < (born.month, born.day))


def main(subject):

    
    
    #run logfilePreparation
    logFile.main(subject)
    
    
    
    inPath = os.path.dirname(os.path.realpath(__file__)) #'/Users/aharms/wdir/language_fmri'
    
    
    
    
    #make directory for subject
    #check if already exist
    if not os.path.isdir(os.path.join(inPath,"FSL_outputs", subject)):
        os.mkdir(os.path.join(inPath,"FSL_outputs", subject))
    
    
    
    
    
    
    subj_dir = inPath + '/subjects/' + subject
    
    for session in os.listdir(subj_dir):
        if (('SPRACHE_VISUELL_3T_0005' in session) or ('sprache_visuell_3T' in session) or ('SPRACHE_VISUELL_3T_0006' in session)) and ('gfm' not in session) and ('GFM' not in session):
                dcm_dir=os.path.join(subj_dir,session)
                #found_t1=True
    
    #dcm2nii
    #checken, ob schon nifit da
    if not any(file.endswith(".nii.gz") for file in os.listdir(subj_dir)):
        out = subprocess.Popen(['dcm2nii -o '+ subj_dir  + ' ' + dcm_dir], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
        out.communicate()
        
        #rename nii
        out = subprocess.Popen(['mv ' + subj_dir + '/*.nii.gz ' + subj_dir + '/' + subject + '.nii.gz'], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
        out.communicate()
    
    
    
    
    
    #get Subject info:
    dcm=os.listdir(dcm_dir)[3]
    
    
    
    #So zB kommt man an das Alter
    #Muss noch gemacht werden für Name, ID etc.... um das im .fsf File zu editieren
    
    #DateOfBirth
    out = subprocess.Popen(['dcmdump '+os.path.join(dcm_dir,dcm)+'| grep "Birth" | cut -c17-24'], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
    stdout,stderr = out.communicate()
    doBirth = stdout.strip()
    
    #DateOfStudy
    out = subprocess.Popen(['dcmdump '+os.path.join(dcm_dir,dcm)+'| grep "StudyDate" | cut -c17-24'], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
    stdout,stderr = out.communicate()
    doStudy = stdout.strip()
    
    #patientName
    out = subprocess.Popen(['dcmdump '+os.path.join(dcm_dir,dcm)+'| grep "PatientName" '], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
    stdout,stderr = out.communicate()
    name = stdout.strip()

    #patientID
    out = subprocess.Popen(['dcmdump '+os.path.join(dcm_dir,dcm)+'| grep "PatientID" '], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
    stdout,stderr = out.communicate()
    identification = stdout.strip()

    
    #RepetitionTime
    out = subprocess.Popen(['dcmdump '+os.path.join(dcm_dir,dcm)+'| grep "RepetitionTime" | cut -c17-20'], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
    stdout,stderr = out.communicate()
    TR = int(stdout.strip())/1000
    TR = '{0:.6f}'.format(TR)
    
    
    #Number of volumes
    # path, dirs, files = next(os.walk(dcm_dir))
    # os.path.listdir(dcm_dir)
    file_count = str(len(os.listdir(dcm_dir)))
    print(dcm_dir)
    print(file_count)
    
    
    #Log_File_info
    LogPath_speech = inPath + '/subjects/' + subject+'/log_speech_'+subject+'.txt'
    LogPath_no_speech = inPath + '/subjects/' + subject+'/log_no_speech_'+subject+'.txt'
    
    
    
    #getDesign.fsf
    
    
    with open(os.path.join("design.fsf")) as f:
        with open(inPath + '/subjects/' + subject+'/'+ "%s.fsf"%(subject), 'w', encoding='utf-8') as outfile: #Der Pfad muss noch angepasst werden!
            line_list = f.readlines()
            
            for line in line_list:
                
                
                #REPLACE output directory
                line = line.replace('FSL_OUTPUT_SUBJ_DIR', os.path.join(inPath,"FSL_outputs", subject)) 
    
                
                #REPLACE TR 
                line = line.replace('REPETITIONTIME', TR) 
    
                
                #REPLACE number of volumes 
                line = line.replace('TOTALVOLUMES', file_count) 
    
    
                #REPLACE name of nifti file 
                line = line.replace('NIFTIFILE', subj_dir+'/'+subject) 
                
                #REPLACE name of nifti file 
                line = line.replace('LOGPATHSPEECH', LogPath_speech)             
                
                #REPLACE name of nifti file 
                line = line.replace('LOGPATHNOSPEECH', LogPath_no_speech) 
                
                outfile.write(line)

    
    
            
            #outfile.write('Hello, world!\n')
            outfile.close()
    

    
    #run feat analysis
    out = subprocess.Popen(['feat '+ inPath + '/subjects/' + subject +'/' +  subject +'.fsf'], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
    out.communicate()
    #This makes the system wait till feat is finished
    out.wait()
    
    
    
    


    #make the masks + screenshots
    out = subprocess.Popen(['~/wdir/language_fmri/scripts/create_masks_in_native_space.sh '+os.path.join(inPath,"FSL_outputs", subject + ".feat")], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
    out.communicate()
    out.wait()


    print()
    print("===================================================")    
    print()   
    print("         Lateralization")
    print()
    print("===================================================")    
    print() 

    #print information
    out = subprocess.Popen(['~/wdir/language_fmri/scripts/LatIndex_Labelwise.sh '+os.path.join(inPath,"FSL_outputs", subject + ".feat")], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
    printout = out.communicate()[0]
    print(printout)


    #age = calculate_age(datetime.datetime.strptime(doBirth, "%Y%m%d"), datetime.datetime.strptime(doStudy, "%Y%m%d"))
    print()
    print("===================================================")    
    print()    
    print("         Language fMRI analysis finished ")
    print()        
    print("===================================================")
    print()
    print("         Patient's name: " + name.split("[")[1].split("]")[0].replace("^",", "))
    print("         Patient's ID: " + identification.split("[")[1].split("]")[0])
    print("         Patietnt's BirthDate: " + datetime.datetime.strptime(doBirth, "%Y%m%d").strftime("%d.%m.%Y"))
    print("         Study Date: " + datetime.datetime.strptime(doStudy, "%Y%m%d").strftime("%d.%m.%Y"))
    print()
    print("===================================================")


    #remove empty folder
    if os.path.exists(os.path.join(inPath,"FSL_outputs", subject)):
        # removing the file using the os.remove() method
        os.rmdir(os.path.join(inPath,"FSL_outputs", subject))





if __name__=='__main__':
    
    a=ArgumentParser(description=version+': Run FSL FEAT analysis for given subject')
    a.add_argument('-s',dest='subject_id',help='subect ID')#,default='Sequenzen_Lub_Epileptologie_Basis')
    args=a.parse_args()
    
    main(args.subject_id)





    